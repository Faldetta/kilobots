#include "witch.h"

REGISTER_USERDATA(USERDATA)

int main() {
    /* initialize the kilobots hw */
    kilo_init();
    /* initialize received message callback */
    kilo_message_rx = rx_message;
    /* initialize sent message callback */
    kilo_message_tx = tx_message;
    /* initialize succesfull sent callback */
    kilo_message_tx_success = tx_message_success;
    /* start the kilobots */
    kilo_start(setup, loop);
    return 0;
}

// executed one time
void setup() {
    initialize_userdata();
    choose_colors();
}

// executed infinite times
void loop() {
    if(!mydata->witch){
        // init
        if(mydata->phase == INIT){
          runner_setup();
          if(mydata->runner_color != 0){
            mydata->phase = DEPARTURE;
          }
        }
        // departure
        if(mydata->phase ==  DEPARTURE){
            move_straight();
            mydata->last_action_time = kilo_ticks;
            mydata->phase = TURN_AROUND;
        // turn around
        } else if(mydata->phase == TURN_AROUND && kilo_ticks - mydata->last_action_time > PHASE_1_DURATION){
            move_right();
            mydata->last_action_time = kilo_ticks;
            mydata->phase = GAME_SETUP;
        // motors cooldown and setup to play the game
        } else if(mydata->phase == GAME_SETUP && kilo_ticks - mydata->last_action_time > PHASE_2_DURATION){
            move_stop();
            mydata->received_message.data[0] = 0;
            mydata->received_distance = 255;
            mydata->phase = GAME_START;
        // start the game with random movements toward the witch
        } else if(mydata->phase == GAME_START){
            rand_move();
            if(runner_message()){
              mydata->phase = TRY_TO_CATCH;
            }
            check_catch();
        // try to catch the runner
        } else if(mydata->phase == TRY_TO_CATCH){
            seek_n_destroy();
        }
        // the game ends after 5 minutes or if the runner is catched
        if(mydata->phase == GAME_END || kilo_ticks > GAME_DURATION){
          set_color(RGB(0,0,0));
          move_stop();
        }
  }
}

// check if the runner is catched
void check_catch(){
  if(mydata->received_distance < MINIMUM_DISTANCE && (mydata->received_message.data[0]) != (RGB(0,0,0))){
    mydata->phase = GAME_END;
  }
}

// seek for the runner and try to catch
void seek_n_destroy(){
  // TODO stops also the catcher
  if(mydata->received_distance < MINIMUM_DISTANCE){
    mydata->phase = GAME_END;
  } else if(mydata->received_distance > mydata->current_distance){
    mydata->chase = !mydata->chase;
  } else if(mydata->received_distance == mydata->current_distance){
    mydata->phase = GAME_SETUP;
  }
  if(!mydata->chase){
    move_right();
  }else{
    move_left();
  }
  mydata->current_distance = mydata->received_distance;
}

// check if the last received message is from the runner
uint8_t runner_message(){
	return mydata->received_message.data[0] == mydata->runner_color ? 1 : 0;
}

// setup the runner and change its color to white
void runner_setup(){
  if((mydata->received_message.data[0]) == (RGB(0,0,0))){
    mydata->runner_color = mydata->received_message.data[1];
    if(mydata->runner_color == mydata->colors[kilo_uid%7]){
      mydata->runner = 1;
      //runner become white
      set_color(RGB(3,3,3));
    }
  }
}

// message received callback
void rx_message(message_t *msg, distance_measurement_t *d) {
  mydata->received_message = *msg;
  if(mydata->phase > 2 && runner_message()){
    mydata->received_distance = estimate_distance(d);
  }
  if(mydata->runner){
     mydata->received_distance = estimate_distance(d);
  }
}

// message transmitted callback
message_t *tx_message() {
    return &(mydata->sent_message);
}

// message transmitted succesfully callback
void tx_message_success() {}

void initialize_userdata() {
    initialize_colors();
    initialize_message();
    mydata->phase = 0;
    mydata->last_action_time = 0;
    if(kilo_uid % 7 == 0) {
        mydata->witch = 1;
    } else {
        mydata->witch = 0;
    }
    mydata->runner = 0;
    mydata->runner_color = 0;
    mydata->received_distance = 255;
    mydata->current_distance = 255;
    mydata->chase = 0;
}

void initialize_colors() {
    mydata->colors[0] = RGB(0,0,0);
    mydata->colors[1] = RGB(3,0,0);
    mydata->colors[2] = RGB(3,3,0);
    mydata->colors[3] = RGB(0,3,0);
    mydata->colors[4] = RGB(0,3,3);
    mydata->colors[5] = RGB(0,0,3);
    mydata->colors[6] = RGB(3,0,3);
}

void initialize_message() {
    uint8_t rand = rand_hard();
    // sender color
    (mydata->sent_message).data[0] = mydata->colors[kilo_uid % 7];
    // if it is the witch sends the runner's color
    if(mydata->witch) {
      // the index of the color can't be 0
      // the witch can't be the runner
      mydata->runner_color = mydata->colors[(rand % 6) + 1];
      (mydata->sent_message).data[1] = mydata->runner_color;
    }
    // set message parameters
    (mydata->sent_message).type = NORMAL;
    (mydata->sent_message).crc = message_crc(&(mydata->sent_message));
}

// choose the kilobot color, hardcoded by ID
void choose_colors() {
    set_color(mydata->colors[kilo_uid%7]);
}

void move_straight() {
    spinup_motors();
    set_motors(kilo_turn_left,kilo_turn_right);

}

void move_right() {
    spinup_motors();
    set_motors(0,kilo_turn_right);
}

void move_left() {
    spinup_motors();
    set_motors(kilo_turn_left,0);
}

void move_stop() {
    set_motors(0,0);
}

void rand_move(){
    uint8_t rand = rand_hard();
    /* choose depending on the seed */
    if(rand % 2) {
        move_left();
    } else {
        move_right();
    }
}
