#include <kilombo.h>

#define INIT 0
#define DEPARTURE 1
#define TURN_AROUND 2
#define GAME_SETUP 3
#define GAME_START 4
#define TRY_TO_CATCH 5
#define GAME_END 6
#define PHASE_1_DURATION 1960
#define PHASE_2_DURATION 265
#define GAME_DURATION 9600
#define MINIMUM_DISTANCE 50

// kilobots data structure
typedef struct{
    // colors array
    uint8_t colors[7];
    // message received_message
    message_t received_message;
    // message to send
    message_t sent_message;
    // phase indicator
    uint8_t phase;
    // kiloticks at last performed action
    uint32_t last_action_time;
    // flag for the witch
    uint8_t witch;
    // flag for the runner
    uint8_t runner;
    // color of the runner
    uint8_t runner_color;
    // distance received with the message
    uint8_t received_distance;
    // previously received distance
    uint8_t current_distance;
    // phase of the chase (0 or 1)
    uint8_t chase;
} USERDATA;

// functions declaration
void setup();
void loop();
void check_catch();
void seek_n_destroy();
uint8_t runner_message();
void runner_setup();
void rx_message(message_t *msg, distance_measurement_t *d);
message_t *tx_message();
void tx_message_success();
void initialize_userdata();
void initialize_colors();
void initialize_message();
void choose_colors();
void move_straight();
void move_right();
void move_left();
void move_stop();
void rand_move();
